'use strict';

/* Filters */

angular.module('testFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
}).filter('userType', function() {
  return function(input) {
      var result = 'Админ';
      if (input == 2) result = 'Специалист';
    return result;
  };
}).filter('caseType', function() {
  return function(input) {
      var result = 'Тип дела 0';
      if (input == 2) result = 'Тип дела 1';
      if (input == 3) result = 'Тип дела 2';
    return result;
  };
});
