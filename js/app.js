'use strict';

/* App Module */

var testApp = angular.module('testApp', [
    'ngRoute',
    'LocalStorageModule'
], function($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8'
});

testApp.config(['$routeProvider', '$httpProvider',
    function($routeProvider ) {
         $routeProvider.
            when('/test', {
                templateUrl: 'partials/test.html',
                controller: 'TestCtrl'
            }).
            otherwise({
                redirectTo: '/test'
            });

    }]);
