'use strict';

/* Controllers */


testApp.controller('TestCtrl',
    function($scope, localStorageService) {


        if (localStorageService.get('contacts')) {
            $scope.contacts = localStorageService.get('contacts');


        } else {


            $scope.contacts = [{ phone: 79896673194, name: 'Джабраил' , last_name: 'Караев', city: 'Каспийск'}]
            localStorageService.set('contacts' , $scope.contacts)

        }
        $scope.create = function (phone_form) {

            var new_row = {name: phone_form.name , last_name: phone_form.last_name, phone: phone_form.phone, city: phone_form.city }
            $scope.contacts.push(new_row)
            localStorageService.set('contacts' , $scope.contacts)

        }

        $scope.delete = function (index) {

            $scope.contacts.splice(index, 1)
            localStorageService.set('contacts' , $scope.contacts)
        }

     });

